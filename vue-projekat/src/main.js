// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias

import Vue from 'vue'
import App from './App'
import store from './components/store'
import VueRouter from 'vue-router'
import router from './router/InitRouter'
import footer from './components/Footer.vue'
import Notifications from 'vue-notification'



Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.component('Footer',footer)
Vue.use(Notifications)


const VueApp = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

export default VueApp













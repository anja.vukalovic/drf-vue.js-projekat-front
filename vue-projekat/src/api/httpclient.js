import axios from "axios";
import app from "../main";
import Vue from 'vue';

const httpClient = axios.create({
  baseURL: 'http://localhost:8000', // Exp. localhost:5000
  headers: {
    "Content-Type": "application/json"
  }
});
// Create a function that checks if there is token in local storage
// If there is no token redirect to login page (check if page is login)
const requestAuthInterceptor = (config) => {
    
    const access = localStorage.getItem("access");
    if(access){
    config.headers.common['Authorization'] = 'Bearer ' + access;}
  return config;
};

// Error Interceptor - processing
const responseErrorInterceptor = (error) => {
  const ignoreRoutes = ["Routes that should be ignored when handling errors"];
  // TODO check some and every functions
  let isIgnored = ignoreRoutes.some(
    function (route) {
      return this[0].includes(route);
    },
    [error.response.config.url]
  );
  switch (error.response.status) {
    case 400:
      // parse BAD REQUEST errors
      Vue.notify({
        group: 'foo',
        type: 'error',
        title: 'Bad Request Error',
        text: 'User with this username or email already exists'
      })
      break;
    case 401: // Unauthorized request
      // Redirect to login and remove token from locale storage
      console.log(error.response.config.url.includes("refresh"));
      const refresh = localStorage.getItem("refresh");
      if (refresh && !error.response.config.url.includes("token")){
       httpClient.post("/account/api/token/refresh/",{refresh})
      .then((response) => {
        if (response.data && response.data.refresh) {
            localStorage.setItem("access", response.data.access);
            localStorage.setItem("refresh", response.data.refresh);
            error.config.headers.common['Authorization'] = 'Bearer ' + response.data.access;
            
          return httpClient.request(error.config)
        }
            })
            ;}
        else {
          //localStorage.removeItem("access");
          //localStorage.removeItem("refresh");
          console.log("sasaas")
          app.$store.dispatch("logout");
          console.log("ispod")
          Vue.notify({
            group: 'foo',
            type: 'error',
            title: 'Wrong username or password',
            text: 'Please try again!'
          })
        }
           
      break;
    case 403: // Forbidden request
      // User is authorized but does not have needed permissions
      Vue.notify({
        group: 'foo',
        type: 'error',
        title: 'Forbidden request',
        text: 'Please try again!'
      })
      break;
    case 500: // Server side errors
    Vue.notify({
      group: 'foo',
      type: 'error',
      title: 'Server side error',
      text: 'Please try again!'
    })
      break;
    default:
    // Default behaviour
  }
  return Promise.reject(error);
};

// Interceptor for responses
const responseInterceptor = (response) => {
  const stateChangeMethods = ["post", "put", "patch", "delete"];
  const ignoreRoutes = ["Routes that should be ignored"];
  // Check if URL contains ignoredRoute
  let isIgnored = ignoreRoutes.some(
    function (route) {
      return this[0].includes(route);
    },
    [response.config.url]
  );
  switch (response.status) {
    case 200:
    case 201:
    case 202:
    case 204:
      // What to do when following codes are returned
      // Exp. Notify user that action has been performed successfully
      break;
    default:
  }
  return response;
};
httpClient.interceptors.request.use(requestAuthInterceptor);
httpClient.interceptors.response.use(
  responseInterceptor,
  responseErrorInterceptor
);
export { httpClient };


import Vuex from "vuex"
import Vue from 'vue'
import {httpClient} from "../api/httpclient"

Vue.use(Vuex)

export default new Vuex.Store(
    {
        state: {
            isAuthenticated: false,
            isFirstTimeLoaded: true,

        },
        mutations: {
            SET_AUTHENTIFICATION(state, status) {
                state.isAuthenticated = status;
            },
            SET_IS_FIRST_TIME_LOADED(state, status){
                state.isFirstTimeLoaded = status;

            }
        },
        actions: {
            setIsAuth({ commit },status) {
                commit('SET_AUTHENTIFICATION', status)
                
            },
            logout({ commit })
            {
                //poslati poziv za logout djangu, staviti isAuth na false, i isprazniti localStorage
                localStorage.removeItem("access");
                localStorage.removeItem("refresh");
                commit('SET_AUTHENTIFICATION', false)

            },
            async isRefreshTokenValid({commit,getters}){
                if(getters.isFirstTimeLoaded){
                    const refresh = localStorage.getItem("refresh");
                    if (refresh){
                       try { 
                           const response =  await httpClient.post("/account/api/token/refresh/",{refresh})
                            if (response.data.refresh) {
                                localStorage.setItem("access", response.data.access);
                                localStorage.setItem("refresh", response.data.refresh);
                                commit('SET_AUTHENTIFICATION', true)
                                }    
                            }
                        catch (error){
                            console.log(error)
                        }
                    }
                    commit("SET_IS_FIRST_TIME_LOADED",false)
                }
            } 
        },
        getters: {
            isAuthenticated: state => state.isAuthenticated,
            isFirstTimeLoaded: state => state.isFirstTimeLoaded}
    }
);
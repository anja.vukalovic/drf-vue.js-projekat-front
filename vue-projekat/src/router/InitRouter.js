import VueRouter from 'vue-router'
import routes from './Routes'
import {httpClient} from '../api/httpclient'
import store from '../components/store';


let a = 2;

// configure router
const router = new VueRouter({
  mode: 'history',
  routes: routes,
  linkActiveClass: 'active',
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

// IsAuthenticated Guard
router.beforeEach(async (to, from, next) => {
  await store.dispatch("isRefreshTokenValid")
  next()
})

export default router
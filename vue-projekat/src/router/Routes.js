// import store from '@store/store'
import store from '../components/store'
// Layout Pages
const PublicPageLayout = () => import('../components/PublicNavBar.vue')
const PrivatePageLayout = () => import('../components/PrivateNavBar.vue')

const HomePage = () => import('../components/Home.vue')

// Lazy Loading Routes - Public Pages
const TheLogin = () => import(/* webpackChunkName: "public" */ '../components/login.vue')

// Error-Pages
const TheNotFound = () => import(/* webpackChunkName: "public" */ '../components/NotFound.vue')

// Lazy Loading Routes - Private Pages
const ThePrivatePage = () => import(/* webpackChunkName: "private" */ '../components/login.vue')

// Use this guard to redirect users from Login and Register pages if already logged in

const Users = () => import(/* webpackChunkName: "private" */ '../components/Users.vue')
const Register = () => import('../components/register.vue')
const TheProfile = () => import('../components/profile.vue')

const DatePicker = () => import('../pages/Datepicker.vue')


const IfNotAuthenticated = (to,from,next) => {
  console.log(store.getters)
  if(store.getters.isAuthenticated) {
    next({path:'/home'})
  }else{
    next()
  }
}


const checkIfAlreadyAuthenticated = (to, from, next) => {
  console.log(store.getters.isAuthenticated);
  if (store.getters.isAuthenticated === false) {
    next({name:"TheLogin"})
  } else {
    next()
    
  }
}

let publicRoutes = [
  {
    path: '',
    name: 'public',
    component: PublicPageLayout,
   
    children: [
      {
        path: '/login',
        name: 'TheLogin',
        components: { default: TheLogin },
        beforeEnter: IfNotAuthenticated
      },
      {
        path:'/register',
        name:'TheRegister',
        component: Register,
        beforeEnter: IfNotAuthenticated
      },
      { path: '',
      name: 'home',
      components: { default: HomePage },
      beforeEnter: IfNotAuthenticated
      
    },

    ]
  }
 
]


// Load each private module
let privateRoutes = [
  {
    path: '',
    name: 'ThePrivatePage',
    redirect: { name: 'ThePrivatePageName' },
    component: PrivatePageLayout,
    children: [
      {
        path: '/users',
        name: 'TheUsers',
        components: { default: Users },
        beforeEnter: checkIfAlreadyAuthenticated
      },
      {
        path: '/home',
        name: 'TheHome',
        components: { default:HomePage },
      },
      {
        path: '/articles',
        name: 'Articles',
        components: { default:DatePicker},
      },
      {
        path: '/profile',
        name: 'Profile',
        components: { default:TheProfile},
      },

    ]
  },
  
]


const routes = [
  ...publicRoutes,
  ...privateRoutes,
  {
    path: '*',
    name: 'TheNotFound',
    component: TheNotFound
  }]

export default routes